var files_dup =
[
    [ "FTerm_IMUdriver.py", "FTerm__IMUdriver_8py.html", "FTerm__IMUdriver_8py" ],
    [ "FTerm_InterfaceTask.py", "FTerm__InterfaceTask_8py.html", [
      [ "FTerm_InterfaceTask.InterfaceTask", "classFTerm__InterfaceTask_1_1InterfaceTask.html", "classFTerm__InterfaceTask_1_1InterfaceTask" ]
    ] ],
    [ "FTerm_main.py", "FTerm__main_8py.html", "FTerm__main_8py" ],
    [ "FTerm_MotorDriver.py", "FTerm__MotorDriver_8py.html", [
      [ "FTerm_MotorDriver.DRV8847", "classFTerm__MotorDriver_1_1DRV8847.html", "classFTerm__MotorDriver_1_1DRV8847" ],
      [ "FTerm_MotorDriver.Motor", "classFTerm__MotorDriver_1_1Motor.html", "classFTerm__MotorDriver_1_1Motor" ]
    ] ],
    [ "FTerm_shares.py", "FTerm__shares_8py.html", [
      [ "FTerm_shares.Share", "classFTerm__shares_1_1Share.html", "classFTerm__shares_1_1Share" ],
      [ "FTerm_shares.Queue", "classFTerm__shares_1_1Queue.html", "classFTerm__shares_1_1Queue" ]
    ] ],
    [ "FTerm_TaskData.py", "FTerm__TaskData_8py.html", [
      [ "FTerm_TaskData.TaskData", "classFTerm__TaskData_1_1TaskData.html", "classFTerm__TaskData_1_1TaskData" ]
    ] ],
    [ "FTerm_taskIMU.py", "FTerm__taskIMU_8py.html", [
      [ "FTerm_taskIMU.taskIMU", "classFTerm__taskIMU_1_1taskIMU.html", "classFTerm__taskIMU_1_1taskIMU" ]
    ] ],
    [ "FTerm_TaskMotor.py", "FTerm__TaskMotor_8py.html", [
      [ "FTerm_TaskMotor.TaskMotor", "classFTerm__TaskMotor_1_1TaskMotor.html", "classFTerm__TaskMotor_1_1TaskMotor" ]
    ] ],
    [ "FTerm_TaskTouch.py", "FTerm__TaskTouch_8py.html", "FTerm__TaskTouch_8py" ],
    [ "FTerm_TouchDriver.py", "FTerm__TouchDriver_8py.html", [
      [ "FTerm_TouchDriver.TouchDriver", "classFTerm__TouchDriver_1_1TouchDriver.html", "classFTerm__TouchDriver_1_1TouchDriver" ]
    ] ],
    [ "Lab1_main.py", "Lab1__main_8py.html", "Lab1__main_8py" ],
    [ "Lab2-4_EncoderDriver.py", "Lab2-4__EncoderDriver_8py.html", [
      [ "Lab2-4_EncoderDriver.EncoderDriver", "classLab2-4__EncoderDriver_1_1EncoderDriver.html", "classLab2-4__EncoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "Lab2-4_InterfaceTask.py", "Lab2-4__InterfaceTask_8py.html", [
      [ "Lab2-4_InterfaceTask.InterfaceTask", "classLab2-4__InterfaceTask_1_1InterfaceTask.html", "classLab2-4__InterfaceTask_1_1InterfaceTask" ]
    ] ],
    [ "Lab2-4_MotorDriver.py", "Lab2-4__MotorDriver_8py.html", [
      [ "Lab2-4_MotorDriver.DRV8847", "classLab2-4__MotorDriver_1_1DRV8847.html", "classLab2-4__MotorDriver_1_1DRV8847" ],
      [ "Lab2-4_MotorDriver.Motor", "classLab2-4__MotorDriver_1_1Motor.html", "classLab2-4__MotorDriver_1_1Motor" ]
    ] ],
    [ "Lab2-4_shares.py", "Lab2-4__shares_8py.html", [
      [ "Lab2-4_shares.Share", "classLab2-4__shares_1_1Share.html", "classLab2-4__shares_1_1Share" ],
      [ "Lab2-4_shares.Queue", "classLab2-4__shares_1_1Queue.html", "classLab2-4__shares_1_1Queue" ]
    ] ],
    [ "Lab2-4_TaskEncoder.py", "Lab2-4__TaskEncoder_8py.html", [
      [ "Lab2-4_TaskEncoder.TaskEncoder", "classLab2-4__TaskEncoder_1_1TaskEncoder.html", "classLab2-4__TaskEncoder_1_1TaskEncoder" ]
    ] ],
    [ "Lab2-4_TaskMotor.py", "Lab2-4__TaskMotor_8py.html", [
      [ "Lab2-4_TaskMotor.TaskMotor", "classLab2-4__TaskMotor_1_1TaskMotor.html", "classLab2-4__TaskMotor_1_1TaskMotor" ]
    ] ],
    [ "Lab2_main.py", "Lab2__main_8py.html", "Lab2__main_8py" ],
    [ "Lab3_main.py", "Lab3__main_8py.html", "Lab3__main_8py" ],
    [ "Lab4_closedloop.py", "Lab4__closedloop_8py.html", [
      [ "Lab4_closedloop.closedloop", "classLab4__closedloop_1_1closedloop.html", "classLab4__closedloop_1_1closedloop" ]
    ] ],
    [ "Lab4_main.py", "Lab4__main_8py.html", "Lab4__main_8py" ],
    [ "TermProjectReport.py", "TermProjectReport_8py.html", null ]
];
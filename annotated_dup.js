var annotated_dup =
[
    [ "FTerm_IMUdriver", null, [
      [ "BNO055", "classFTerm__IMUdriver_1_1BNO055.html", "classFTerm__IMUdriver_1_1BNO055" ]
    ] ],
    [ "FTerm_InterfaceTask", null, [
      [ "InterfaceTask", "classFTerm__InterfaceTask_1_1InterfaceTask.html", "classFTerm__InterfaceTask_1_1InterfaceTask" ]
    ] ],
    [ "FTerm_MotorDriver", null, [
      [ "DRV8847", "classFTerm__MotorDriver_1_1DRV8847.html", "classFTerm__MotorDriver_1_1DRV8847" ],
      [ "Motor", "classFTerm__MotorDriver_1_1Motor.html", "classFTerm__MotorDriver_1_1Motor" ]
    ] ],
    [ "FTerm_shares", null, [
      [ "Queue", "classFTerm__shares_1_1Queue.html", "classFTerm__shares_1_1Queue" ],
      [ "Share", "classFTerm__shares_1_1Share.html", "classFTerm__shares_1_1Share" ]
    ] ],
    [ "FTerm_TaskData", null, [
      [ "TaskData", "classFTerm__TaskData_1_1TaskData.html", "classFTerm__TaskData_1_1TaskData" ]
    ] ],
    [ "FTerm_taskIMU", null, [
      [ "taskIMU", "classFTerm__taskIMU_1_1taskIMU.html", "classFTerm__taskIMU_1_1taskIMU" ]
    ] ],
    [ "FTerm_TaskMotor", null, [
      [ "TaskMotor", "classFTerm__TaskMotor_1_1TaskMotor.html", "classFTerm__TaskMotor_1_1TaskMotor" ]
    ] ],
    [ "FTerm_TaskTouch", null, [
      [ "TaskTouch", "classFTerm__TaskTouch_1_1TaskTouch.html", "classFTerm__TaskTouch_1_1TaskTouch" ]
    ] ],
    [ "FTerm_TouchDriver", null, [
      [ "TouchDriver", "classFTerm__TouchDriver_1_1TouchDriver.html", "classFTerm__TouchDriver_1_1TouchDriver" ]
    ] ],
    [ "Lab2-4_EncoderDriver", null, [
      [ "EncoderDriver", "classLab2-4__EncoderDriver_1_1EncoderDriver.html", "classLab2-4__EncoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "Lab2-4_InterfaceTask", null, [
      [ "InterfaceTask", "classLab2-4__InterfaceTask_1_1InterfaceTask.html", "classLab2-4__InterfaceTask_1_1InterfaceTask" ]
    ] ],
    [ "Lab2-4_MotorDriver", null, [
      [ "DRV8847", "classLab2-4__MotorDriver_1_1DRV8847.html", "classLab2-4__MotorDriver_1_1DRV8847" ],
      [ "Motor", "classLab2-4__MotorDriver_1_1Motor.html", "classLab2-4__MotorDriver_1_1Motor" ]
    ] ],
    [ "Lab2-4_shares", null, [
      [ "Queue", "classLab2-4__shares_1_1Queue.html", "classLab2-4__shares_1_1Queue" ],
      [ "Share", "classLab2-4__shares_1_1Share.html", "classLab2-4__shares_1_1Share" ]
    ] ],
    [ "Lab2-4_TaskEncoder", null, [
      [ "TaskEncoder", "classLab2-4__TaskEncoder_1_1TaskEncoder.html", "classLab2-4__TaskEncoder_1_1TaskEncoder" ]
    ] ],
    [ "Lab2-4_TaskMotor", null, [
      [ "TaskMotor", "classLab2-4__TaskMotor_1_1TaskMotor.html", "classLab2-4__TaskMotor_1_1TaskMotor" ]
    ] ],
    [ "Lab4_closedloop", null, [
      [ "closedloop", "classLab4__closedloop_1_1closedloop.html", "classLab4__closedloop_1_1closedloop" ]
    ] ]
];